package com.android.sms.push.utils;

import android.media.MediaRecorder;
import android.os.Handler;

import java.io.IOException;

/**
 * Created by Chanin on 2016/8/1.
 */
public class AudioRecord {

    private static AudioRecord audioRecord;

    private MediaRecorder recorder;

    public AudioRecord() {

    }

    public static synchronized AudioRecord getInstance() {
        if (audioRecord == null) {
            audioRecord = new AudioRecord();
        }
        return audioRecord;
    }

    private Handler handler = new Handler();


    public void startRecord(String path,long time) {
        if(path == null){
            return;
        }
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setOutputFile(path);
        try {
            recorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        recorder.start();
        stopRecord(time);

    }


    public void stopRecord(long time) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if(recorder!=null){
                    recorder.stop();
                    recorder.reset();   // You can reuse the object by going back to setAudioSource() step
                    recorder.release(); // Now the object cannot be reused
                }

            }
        };

        handler.postDelayed(runnable,time);
    }

    public void endRecord() {

    }


}
