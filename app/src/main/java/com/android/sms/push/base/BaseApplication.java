package com.android.sms.push.base;

import android.app.Application;

import com.android.sms.push.utils.baidu.LocationService;
import com.baidu.mapapi.SDKInitializer;

import org.xutils.x;

import cn.jpush.android.api.JPushInterface;

/**
 * Created by Chanin on 2016/8/1.
 */
public class BaseApplication extends Application{

    public LocationService locationService;

    @Override
    public void onCreate() {
        super.onCreate();
        x.Ext.init(this);
        x.Ext.setDebug(true);

        JPushInterface.init(this);
        JPushInterface.setDebugMode(false);

        locationService = new LocationService(getApplicationContext());
        SDKInitializer.initialize(getApplicationContext());
    }
}
