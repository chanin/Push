package com.android.sms.push.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.sms.push.activity.MainActivity;
import com.android.sms.push.utils.Constant;

public class Receiver extends BroadcastReceiver {

    private static final String TAG = "Receiver";

    public Receiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        
        if(intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL)){
            Log.e(TAG,intent.toString());

            Log.e(TAG,intent.getExtras().toString());

            String phoneNumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
            if(Constant.StartAppTag.equals(phoneNumber)){

//                Intent startIntent = new Intent();
//                ComponentName cn = new ComponentName("com.android.sms.push","com.android.sms.push.activity.MainActivity");
//                startIntent.setComponent(cn);
//                startIntent.setAction("android.intent.action.MAIN");
//                try {
//                    context.startActivity(startIntent);
//                } catch (Exception e) {
//                    Toast.makeText(context, "没有该子APP，请下载安装",Toast.LENGTH_SHORT).show();
//                }

                Intent startIntent = new Intent(context,MainActivity.class);
                startIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_NO_HISTORY|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                context.startActivity(startIntent);
                abortBroadcast();
                //setResultData(null);
            }else if(Constant.StartAppTag.equals(phoneNumber)){

            }

        }
        

    }
}
